package com.company;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;
import java.util.zip.CRC32;

class CrcCalculator {

    private static final Logger LOGGER = Logger.getLogger(CrcCalculator.class.getName());

    static String checksumInputStream(String filepath) {
        try {
            InputStream inputStreamn = new FileInputStream(filepath);
            CRC32 crc = new CRC32();
            int cnt;
            while ((cnt = inputStreamn.read()) != -1) {
                crc.update(cnt);
            }
            return Long.toHexString(crc.getValue()).toUpperCase();
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
}
