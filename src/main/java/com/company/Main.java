package com.company;

import com.company.Crypto.CryptoTool;

import java.io.File;
import java.util.logging.Logger;

public class Main {


    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        String action = args[0];
        String key = args[1];
        File file = new File(args[2]);
        File fileEncypted = new File("src/file.crypt");
        File fileDecrypted = new File("src/file.decrypt");

        LOGGER.info("action: " + action + " password: " + key);
        LOGGER.info("Crc: " + CrcCalculator.checksumInputStream(args[2]));

        switch (args[0]) {
            case "encrypt": {
                try {
                    CryptoTool.encrypt(key, file, fileEncypted);
                    LOGGER.info("Archivo file.crypt creado con exito");
                } catch( Exception e) {
                    LOGGER.warning("Error tratando de encriptar archivo");
                    LOGGER.warning(e.getMessage());
                    e.printStackTrace();
                }
            }
            break;
            case "decrypt": {
                try {
                    CryptoTool.decrypt(key, file, fileDecrypted);
                    LOGGER.info("Archivo file.decrypt creado con exito");
                } catch( Exception e) {
                    LOGGER.warning("Error tratando de desencriptar archivo");
                    LOGGER.warning(e.getMessage());
                    e.printStackTrace();
                }
            }
            break;
            default:
        }
    }
}
